#include "tree.h"
#include "defines.h"

Tree_node *tn_new_node(int data) {
    Tree_node *ret = malloc(sizeof(Tree_node));
    ret->l = ret->r = ret->d = NULL;
    ret->isvar = 0;
    ret->data = data;
    return ret;
}


int tn_get_num(Tree_node *root, char **var, int *num) {
    Tree_node *temp;
    while (**var != '\0') {
        if (!root)
            return NAN;
        if (root->data > **var)
            root = root->l;
        else if (root->data < **var)
            root = root->r;
        else {
            ++*var;
            temp = root;
            root = root->d;
        }
    }
    if (!temp || !temp->isvar)
        return NAN;
    *num = temp->num;
    return NUM;
}

void tn_assign_num(Tree_node **root, char *var, int num) {
    Tree_node *prev;
    Tree_node *node;
    if (*root == NULL)
        *root = tn_new_node(*var);
    node = *root;
    for (;;) {
        prev = node;
        if (node->data > *var) {
            if (node->l == NULL)
                node->l = tn_new_node(*var);
            node = node->l;
        }
        else if (node->data < *var) {
            if (node->r == NULL)
                node->r = tn_new_node(*var);
            node = node->r;
        }
        else {
            if (*(++var) == '\0') /* done */
                break;
            if (node->d == NULL)
                node->d = tn_new_node(*var);
            node = node->d;
        }
    }
    node->num = num;
    node->isvar = 1;
    return;
}
