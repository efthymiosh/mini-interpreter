#ifndef TREE_H
#define TREE_H

#include <stdlib.h>

typedef struct Tree_node Tree_node;
struct Tree_node {
    Tree_node *l;
    Tree_node *r;
    Tree_node *d;
    int data;
    int isvar;
    int num;
};

Tree_node *tn_new_node(int data);

int tn_get_num(Tree_node *root, char **var, int *num);
void tn_assign_num(Tree_node **root, char *var, int num);

#endif
