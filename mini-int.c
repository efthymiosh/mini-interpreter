#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tree.h"
#include "defines.h"

#define NMEMB_AMT 128
#define IPTRS_SZ 128

#define SKIP_SPACES(X) while(*(X) == ' ') ++X;
#define SKIP_RESULTVAR(X) while(*(X) != '=') ++X;
#define ABORT(str, ...) {fprintf(stderr, str, __VA_ARGS__); exit(EXIT_FAILURE);}

static int parse_num(char **numstr, int *parsed) {
    if ((**numstr > '9') || (**numstr < '0'))
        return NAN;
    *parsed = 0;
    do {
        *parsed *= 10;
        *parsed += **numstr - '0';
        ++*numstr;
    } while ((**numstr <= '9') && (**numstr >= '0'));
    return NUM;
}

static int parseval_var(char **numstr, int *val, Tree_node **map) {
    char temp, *end = *numstr;
    int ret;
    while (*(++end) != ' ' && *end != '\n');
    temp = *end;
    *end = '\0';
    ret = tn_get_num(*map, numstr, val);
    *end = temp;
    return ret;
}

static void save_val(char *var, int val, Tree_node **map) {
    char *end = var;
    while (*(++end) != ' ');
    *end = '\0';
    tn_assign_num(map, var, val);
    *end = ' ';
    return;
}

int main(int argc, char **argv) {
    int i, offset = 0, instr_amt = 1, iptrs_sz = IPTRS_SZ, pc;
    int mark_next = 0, buf_size;
    size_t read_amt;
    char *buf, **iptrs;
    FILE *infd = stdin;
    Tree_node *map = NULL;

    if (argc == 2)
        infd = fopen(argv[1], "r");

    /* Read source file to memory */
    if (!(iptrs = malloc(IPTRS_SZ * sizeof(char *))))
        return EXIT_FAILURE;
    if (!(buf = malloc(NMEMB_AMT * sizeof(char))))
        return EXIT_FAILURE;

    buf_size = NMEMB_AMT;
    iptrs[0] = buf;

    while((read_amt = fread(buf + offset, sizeof(char), NMEMB_AMT, infd)) == NMEMB_AMT) {
        if (!(buf = realloc(buf, buf_size)))
            return EXIT_FAILURE;
        mark_next = 0;
        for (i = offset; i < buf_size; ++i) {
            if (buf[i] == '\n')
                mark_next = 1;
            else if (mark_next) {
                mark_next = 0;
                iptrs[instr_amt++] = buf + i;
                if (instr_amt == iptrs_sz) {
                    iptrs_sz += IPTRS_SZ;
                    if (!(iptrs = realloc(iptrs, iptrs_sz * sizeof(char *))))
                        return EXIT_FAILURE;
                }
            }
        }
        buf_size += NMEMB_AMT;
        offset += NMEMB_AMT;
    }
    buf_size += read_amt - NMEMB_AMT;
    mark_next = 0;
    for (i = offset; i < buf_size; ++i) {
        if (buf[i] == '\n')
            mark_next = 1;
        else if (mark_next) {
            mark_next = 0;
            iptrs[instr_amt++] = buf + i;
            if (instr_amt == iptrs_sz) {
                iptrs_sz += IPTRS_SZ;
                if (!(iptrs = realloc(iptrs, iptrs_sz * sizeof(char *))))
                    return EXIT_FAILURE;
            }
        }
    }

    /* Main interpreter loop */
    for (pc = 1;;) {
        char *instr;
        asm volatile ("nopl 0x11111111\n\t");
        if (!(instr = iptrs[pc - 1]))
            ABORT("%d: Reached end without EXIT statement.\n", pc);
        if (!strncmp(instr, "GOTO", 4)) { /* A jump statement */
            int prev_pc = pc;
            instr += 4;
            SKIP_SPACES(instr);
            if (parse_num(&instr, &pc) == NAN)
                parseval_var(&instr, &pc, &map);
            if (pc > instr_amt)
                ABORT("%-4d: Program Counter out of bounds after GOTO statement\n", prev_pc);
            continue;
        }
        else if (!strncmp(instr, "EXIT", 4)) /* An exit statement */
            break;
        else if (!strncmp(instr, "PRINT", 5)) { /* A print statement */
            instr += 5;
            SKIP_SPACES(instr);
            while (*instr != '\n') { /* while instruction is not over. */
                int val;
                if ((parse_num(&instr, &val) == NAN) && parseval_var(&instr, &val, &map) == NAN)
                    ABORT("%-4d: Using unassigned var\n", pc);
                printf(" %d", val);
                SKIP_SPACES(instr);
            }
            putchar('\n');
        }
        else if (!strncmp(instr, "BZERO", 5)) { /* A branch on zero statement */
            int val;
            instr +=5;
            SKIP_SPACES(instr);
            if ((parse_num(&instr, &val) == NAN) && parseval_var(&instr, &val, &map) == NAN)
                ABORT("%-4d: Using unassigned var\n", pc);
            SKIP_SPACES(instr);
            if (val == 0) {
                int prev_pc = pc;
                if (parse_num(&instr, &pc) == NAN)
                    parseval_var(&instr, &pc, &map);
                if (pc > instr_amt)
                    ABORT("%-4d: Program Counter out of bounds after BZERO statement\n", prev_pc);
                continue;
            }
        }
        else { /* A variable operation and assignment statement */
            char *resultvar = instr;
            int val, val2, op;
            SKIP_RESULTVAR(instr);
            ++instr;
            SKIP_SPACES(instr);
            if ((parse_num(&instr, &val) == NAN) && parseval_var(&instr, &val, &map) == NAN)
                ABORT("%-4d: Using unassigned var\n", pc);
            SKIP_SPACES(instr);
            op = *instr;
            if (op != '\n') { /* No operation, just assignment */
                ++instr;
                SKIP_SPACES(instr);
                if ((parse_num(&instr, &val2) == NAN) && parseval_var(&instr, &val2, &map) == NAN)
                    ABORT("%-4d: Using unassigned var\n", pc);
                switch (op) {
                    case '+': val += val2; break;
                    case '-': val -= val2; break;
                    case '*': val *= val2; break;
                    case '/': val /= val2; break;
                }
            }
            save_val(resultvar, val, &map);
        }
        ++pc;
    }
    asm volatile ("nopl 0x22222222\n\t");
    return EXIT_SUCCESS;
}
